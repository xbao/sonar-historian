import Sonar_historian.Logger.LOG_DEBUG
import Sonar_historian.Logger.LOG_VERSBOSE
import Sonar_historian.Logger.logd
import Sonar_historian.Logger.logv
import Sonar_historian.ScriptArgument.Companion.KEY_MAP
import Sonar_historian.ScriptArgument.Companion.REQUIRED_ARGS
import java.io.File
import kotlin.system.exitProcess

if (args.isEmpty()) {
    printUsage()
    exitProcess(0)
}

var parsedArgs: Map<ScriptArgument, ParsedArgument> = emptyMap()
try {
    parsedArgs = parseArgs(args).associateBy { it.argument }
} catch (e: InvalidArgException) {
    System.err.println(e.toString())
    printUsage()
    exitProcess(1)
}
if (parsedArgs[ScriptArgument.HELP] != null) {
    printUsage()
    exitProcess(0)
}

if (ProcessBuilder("sonar-scanner", "-h")
        .start()
        .waitFor() != 0) {
    System.err.println("ERROR: 'sonar-scanner' must be in the PATH. Is it installed?")
    exitProcess(1)
}

LOG_DEBUG = parsedArgs[ScriptArgument.DEBUG] != null
LOG_VERSBOSE = parsedArgs[ScriptArgument.VERBOSE] != null

val sonarRun = SonarRun(Config(server = parsedArgs.valueOrDefault(ScriptArgument.SERVER)!!,
        sonarqubeProjectKey = parsedArgs.requiredValue(ScriptArgument.SONARQUBE_KEY),
        projectDir = parsedArgs.valueOrDefault(ScriptArgument.PROJECT_DIR)!!,
        appDir = parsedArgs.valueOrDefault(ScriptArgument.APP_DIR)!!,
        lintPath = parsedArgs.valueOrDefault(ScriptArgument.LINT_PATH)!!
))
val revisions = parsedArgs.requiredValue(ScriptArgument.GIT_REVISIONS)
revisions.split(",").map { it.trim() }
        .forEach { revision ->
    sonarRun.run(revision)
}
exitProcess(0)

fun Map<ScriptArgument, ParsedArgument>.requiredValue(scriptArgument: ScriptArgument): String {
    return get(scriptArgument)!!.value!!
}

fun Map<ScriptArgument, ParsedArgument>.valueOrDefault(scriptArgument: ScriptArgument): String? {
    return get(scriptArgument)?.value ?: scriptArgument.default
}


data class Config(val server: String,
                  val sonarqubeProjectKey: String,
                  val projectDir: String,
                  val appDir: String,
                  val lintPath: String)

class SonarRun(val config: Config) {

    val logFiles = mutableListOf<File>()

    fun run(revision: String) {
        println("Analysing $revision")
        logv { "config=$config" }
        logv { "checking out $revision" }
        val projectDir = File(config.projectDir)
        if (!projectDir.isDirectory) {
            throw IllegalArgumentException("file $projectDir is not a directory")
        }

        checkout(revision, projectDir, createLog("$revision.checkout.log"))

        logv { "building the project at $projectDir" }
        build(projectDir, createLog("$revision.gradlew.log"))
        logv{"Running sonar for revision $revision"}
        sendToSonar(projectDir, revision, createLog("$revision.sonar.log"))
        logFiles.forEach { it.delete() }
        logFiles.clear()
        println("Revision $revision sent to sonar server")
    }

    private fun createLog(name: String): File {
        val file = File("/tmp/$name")
        if (file.exists()) file.delete()
        if (!file.createNewFile()) {
            throw RuntimeException("Can't create temp file for logs. path=${file.path}")
        }
        logFiles.add(file)
        return file
    }

    private fun existingDirectories(baseDir: File, vararg dirs: String): List<String> {
        return dirs.filter { File(baseDir, it).isDirectory }
    }

    private fun sendToSonar(projectDir: File, revision: String, sonarLog: File) {
        val date = getGitDate(projectDir)
        logv { "date=$date" }

        val appDir = File(projectDir, config.appDir)
        if (!appDir.isDirectory) throw RuntimeException("appDir is not a directory. path=${appDir.path}")

        val sourceDirs = existingDirectories(appDir,
                "src/main/AndroidManifest.xml",
                "src/main/assets",
                "src/main/java",
                "src/main/res")
        val testDirs = existingDirectories(appDir,
                "src/test/java",
                "src/test/resources")
        val classDirs = existingDirectories(appDir,
                "build/intermediates/classes/debug",
                "build/tmp/kotlin-classes/debug")

        val testClassDirs = existingDirectories(appDir,
                "build/intermediates/classes/test/debug",
                "build/tmp/kotlin-classes/debugUnitTest")
        val jacocoReportPath = if (File(appDir, "build/jacoco/testDebugUnitTest.exec").exists()) {
            "build/jacoco/testDebugUnitTest.exec"
        } else {
            ""
        }

        val sonarArgs = listOf(
                SonarArg("sonar.host.url", config.server),
                SonarArg("sonar.projectBaseDir", config.appDir),
                SonarArg("sonar.projectKey", config.sonarqubeProjectKey),
                SonarArg("sonar.projectDate", date),
                SonarArg("sonar.sources", sourceDirs),
                SonarArg("sonar.tests", testDirs),
                SonarArg("sonar.java.binaries", classDirs),
                SonarArg("sonar.java.test.binaries", testClassDirs),
                SonarArg("sonar.jacoco.reportPaths", jacocoReportPath),
                SonarArg("sonar.android.lint.reports", config.lintPath),
                SonarArg("sonar.projectVersion", revision),
                SonarArg("sonar.verbose", "true")
        ).filter {
            if (it.value.isBlank()) {
                println("Warning: property ${it.property} is blank and will be ignored")
            }
            return@filter it.value.isNotBlank()
        }.map { it.toString() }.toTypedArray()

        listOf("sonar-scanner", *sonarArgs).runProcess(projectDir, sonarLog)
    }

    private fun getGitDate(projectDir: File): String {
        val process = ProcessBuilder("git log -n 1 --format=%cd --date=short".split(" "))
                .directory(projectDir)
                .start()
        val returnCode = process.waitFor()
        if (returnCode != 0) {
            throw RuntimeException("failed to get git date")
        }
        return process.inputStream.bufferedReader().use { it.readText() }
    }


    private fun build(directory: File, logFile: File) {
        val gradlew = File(directory, "gradlew")
        if (!gradlew.isFile) {
            throw IllegalArgumentException("cannot find gradlew at $gradlew")
        }
        try {
            "./gradlew clean check".runProcess(directory, logFile)
        } catch(e: ProcessErrorException) {
            println("build failed, rerunning excluding lint")
            "./gradlew clean check -x lint".runProcess(directory, logFile)
        }
    }

    private fun checkout(revision: String, directory: File, logFile: File) {
        "git checkout $revision".runProcess(directory, logFile)
    }

    data class SonarArg(val property: String, val value: String) {

        constructor(property: String, values: List<String>) : this(property, values.joinToString(","))

        override fun toString(): String {
            return "-D$property=$value"
        }
    }

    companion object {
        fun String.runProcess(directory: File, logFile: File) {
            split(" ").runProcess(directory, logFile)
        }

        fun List<String>.runProcess(directory: File, logFile: File) {
            logd { "CMD:${this.joinToString(",")}" }
            val process = ProcessBuilder(this)
                    .directory(directory)
                    .start()
            process.inputStream.bufferedReader().lineSequence()
                    .forEach { line ->
                        logd { line }
                        logFile.appendText(line)
                    }
            process.errorStream.bufferedReader().lineSequence()
                    .forEach { line ->
                        logd { line }
                        logFile.appendText(line)
                    }

            if (process.waitFor() != 0) {
                throw ProcessErrorException("Failed to run command. arguments=$this, directory=$directory, logFile=$logFile")
            }

        }
    }

    class ProcessErrorException(message: String) : Exception(message)
}

object Logger {

    var LOG_DEBUG = false
    var LOG_VERSBOSE = false
    inline fun logd(messageMaker: () -> String) {
        if (LOG_DEBUG) {
            println("d | ${messageMaker()}")
        }
    }

    inline fun logv(messageMaker: () -> String) {
        if (LOG_VERSBOSE || LOG_DEBUG) {
            println("v | ${messageMaker()}")
        }
    }
}

fun printUsage() {
    println("""Usage: kotlinc -script sonar-historian.kts -- <args>
        |arguments:""".trimMargin())

    ScriptArgument.values().forEach {
        val specifier = if (it.shortKey != null) "${it.shortKey}|${it.key}" else it.key +
                if (it.hasValue) " <value>" else ""
        val required = if (it.isRequired) "(required)" else ""
        val default = if (it.default != null) "(default=${it.default})" else ""
        println("""${listOf(specifier, required, default).filter { it.isNotEmpty() }.joinToString(" ")}
            |   ${it.description}
        """.trimMargin())
    }
}

fun parseArgs(args: Array<String>): List<ParsedArgument> {
    val results = mutableListOf<ParsedArgument>()
    var index = 0
    while (index < args.size) {
        val key = args[index]
        index++
        val argument = KEY_MAP[key]
        if (argument == null) {
            throw InvalidArgException("Unknown key :$key")
        }
        if (argument.hasValue) {
            if (index > args.size || KEY_MAP.containsKey(args[index])) {
                throw InvalidArgException("Key requires value: $key")
            }
            val value = args[index]
            index++
            results.add(ParsedArgument(argument, value))
        } else {
            results.add(ParsedArgument(argument, null))
        }
    }

    val missingArgs = REQUIRED_ARGS.minus(results.map { it.argument })
    if (missingArgs.isNotEmpty()) {
        throw InvalidArgException("Missing required arguments: ${missingArgs.joinToString(",") { it.key }}")
    }
    return results
}

class InvalidArgException(message: String) : RuntimeException(message)

enum class ScriptArgument(val key: String, val description: String, val shortKey: String? = null, val isRequired: Boolean = false, val hasValue: Boolean, val default: String? = null) {
    SONARQUBE_KEY(
            key = "--key",
            description = "The Sonarqube key for the project. If it does not exist the server will create a new project for it.",
            isRequired = true,
            hasValue = true
    ),
    GIT_REVISIONS(
            key = "--revisions",
            description = """The comma separated list of git tags to build and analyse.
                |       MUST BE IN CHRONOLOGICAL ORDER. Sonarqube only allows strictly increasing dates.
                |       The version name of the analysis is set to the revision.
                |       Tags with /'s or special characters are not supported
            """.trimMargin(),
            isRequired = true,
            hasValue = true
    ),
    SERVER(
            key = "--server",
            description = "The http address and port of the sonarqube server",
            isRequired = false,
            hasValue = true,
            default = "http://localhost:9000"
    ),
    PROJECT_DIR(
            key = "--project-dir",
            shortKey = "-p",
            description = "The root directory of the project where 'gradlew' lives. This is usually the git repository.",
            hasValue = true,
            default = "."
    ),
    APP_DIR(
            key = "--app-dir",
            description = "The subdirectory of <PROJECT_DIR> that contains the apk module",
            hasValue = true,
            default = "app"
    ),
    LINT_PATH(
            key = "--lint-path",
            shortKey = "-l",
            description = "The subdirectory of <APP_DIR> where the lint results will be",
            hasValue = true,
            default = "build/outputs/lint-results.xml"
    ),
    DEBUG(
            key = "--debug",
            shortKey = null,
            description = "Enables all of the logging",
            hasValue = false,
            default = null
    ),
    VERBOSE(
            key = "--verbose",
            shortKey = null,
            description = "Enables more verbose logging",
            hasValue = false,
            default = null
    ),
    HELP(
            key = "--help",
            shortKey = "-h",
            description = "This help screen",
            hasValue = false
    ), ;

    companion object {
        val KEY_MAP: Map<String, ScriptArgument>
        val REQUIRED_ARGS: List<ScriptArgument> = values().filter { it.isRequired }

        init {
            val map = mutableMapOf<String, ScriptArgument>()
            values().associateByTo(map, { it.key })
            values().filter { it.shortKey != null }.associateByTo(map, { it.shortKey!! })
            KEY_MAP = map
        }
    }
}

data class ParsedArgument(val argument: ScriptArgument, val value: String?)